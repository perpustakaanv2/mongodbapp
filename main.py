from fastapi import FastAPI
from books_route import router as books_router
from pydantic import BaseModel
#import csv
#from pymongo import MongoClient


app = FastAPI()

app.include_router(books_router)



@app.get("/")
async def read_main():
    return {"message": "Hello Bigger Applications!"}

